(function() {
	var triggerBttn = $('#trigger-overlay'), 
		overlay = $('.overlay'), 
		navLink = $('.nav__link'),
		transEndEventNames = {
			'WebkitTransition': 'webkitTransitionEnd',
			'MozTransition': 'transitionend',
			'OTransition': 'oTransitionEnd',
			'msTransition': 'MSTransitionEnd',
			'transition': 'transitionend'
		},
		transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ],
		support = { transitions : Modernizr.csstransitions };

	function toggleOverlay() {

		if( overlay.hasClass('open') ) {
			
			overlay.removeClass( 'open' );
			overlay.addClass( 'close' );

			var onEndTransitionFn = function( ev ) {
				if( support.transitions ) {
					if( ev.propertyName !== 'visibility' ) return;
					this.unbind( transEndEventName, onEndTransitionFn );
				} 
			};

			if( support.transitions ) {
				overlay.bind( transEndEventName, onEndTransitionFn );
			}
			else {
				onEndTransitionFn();
			}
			overlay.removeClass( 'close' );
			// triggerBtn.removeClass('open');
		}
		else if( overlay.hasClass( 'close' ) ) {
			overlay.removeClass( 'close' );
			// triggerBtn.removeClass('open');
		} 
		else if( !overlay.hasClass('open') && !overlay.hasClass('close') ) {
			overlay.addClass( 'open' );
		}
	}

	function toggleTrigger() {
		$('.navicon-button').removeClass('open');
	}

	triggerBttn.click( toggleOverlay );
	navLink.click( toggleOverlay );
	navLink.click( toggleTrigger );
	overlay.click( function () {$(this).removeClass('open') });
	overlay.click( toggleTrigger );
})();